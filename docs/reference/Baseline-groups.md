# Baseline Groups

RenoirSt includes the following groups in its Baseline that can be used as
loading targets:

- `Deployment` will load all the packages needed in a deployed application
- `Tests` will load the test cases
- `Tools` will load the extensions to the SUnit framework and development tools
  (inspector and spotter extensions)
- `CI` is the group loaded in the continuous integration setup
- `Development` will load all the needed packages to develop and contribute to
  the project
- `Deployment-Seaside-Extensions` will load all the packages needed in a deployed
  application including the Javascript extensions
- `Development-Seaside-Extensions` will load all the needed packages to develop
  and contribute to the project including the Javascript extensions
