# RenoirSt Documentation

A DSL enabling programmatic cascading style sheet generation for Pharo Smalltalk.

To learn about the project, [install it](how-to/how-to-load-in-pharo.md) and
follow the [tutorial](tutorial/Tutorial-TOC.md).

---

To use the project as a dependency of your project, take a look at:

- [How to use RenoirSt as a dependency](how-to/how-to-use-as-dependency-in-pharo.md)
- [Baseline groups reference](reference/Baseline-groups.md)
